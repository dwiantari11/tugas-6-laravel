<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function utama()
    {
        return view('home');
    }

    public function tabel()
    {
        return view('table');
    }
}
