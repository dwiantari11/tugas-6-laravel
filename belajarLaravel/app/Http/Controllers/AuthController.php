<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('form');
    }

    public function kirim(Request $request)
    {
        $namadepan = $request['first_name'];
        $namabelakang = $request['last_name'];

        return view('welcome', compact('namadepan', 'namabelakang'));

    }
}
