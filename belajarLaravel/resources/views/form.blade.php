@extends('layout.master')

    @section('judul')
        Buat Account Baru!
    @endsection

    @section('content')

    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label> <br> <br>
        <input type="text" name="first_name"> <br> <br>
        <label>Last name:</label> <br> <br>
        <input type="text" name="last_name"> <br> <br>
        <label>Gender:</label> <br> <br>
        <input type="radio" name="gender" value="1"> Male <br>
        <input type="radio" name="gender" value="2"> Female <br>
        <input type="radio" name="gender" value="3"> Other <br> <br>
        <label>Nationality:</label> <br> <br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singapura</option>
            <option value="3">Malaysia</option>
        </select> <br> <br>
        <label>Language Spoken:</label> <br> <br>
        <input type="checkbox" name="language_spoken" value="1"> Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken" value="2"> English <br>
        <input type="checkbox" name="language_spoken" value="3"> Other <br> <br>
        <label>Bio:</label> <br> <br>
        <textarea name="bio" cols="20" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">

    </form>
    @endsection
    
