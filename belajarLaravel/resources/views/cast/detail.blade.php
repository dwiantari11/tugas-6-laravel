@extends('layout.master')

    @section('judul')
        Halaman Detail Kategori
    @endsection

    @section('content')

        <h1>{{$castDetail->nama}}</h1>
        <h3>{{$castDetail->umur}}</h3>
        <p>{{$castDetail->bio}}</p>

        <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
   
    @endsection
    