@extends('layout.master')

    @section('judul')
        Halaman Detail Film
    @endsection

    @section('content')
    

    <div class="row">
     
       
            <div class="card">
                <img src="{{asset('image/'. $film->poster)}}" style="width: 100vh; height: 400px" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5>{{$film->judul}}</h5>
                    <h6>Tahun: {{$film->tahun}}</h6>
                    <p class="card-text">{{$film->ringkasan,20}}</p>
                    <a href="/film" class="btn btn-info btn-sm">Kembali</a>
            
                </div>
            </div>
    </div>    
            
    <h1>List Kritik</h1>
    <!-- List Kritik -->
    <div class="row">
        @forelse ($film->kritik as $item)
        <div class="card" style="width: 50rem; height: 20rem;">
            <div class="card-header">
                {{$item->user->name}}
            </div>
            <div class="card-body">
                <h2 class="text-warning">Point {{$item->point}}/5</h2>
                <p class="card-text">{{$item->content}}</p>
            </div>
        </div>
        @empty
            <h2>Belum Ada Kritik</h2>
        @endforelse
    </div>
    <div class="row">
            <!-- Form Tambah Kritik -->
            <div class="card" style="width: 50rem; height: 25rem;">
                <form action="/kritik" method="POST" >
                    @csrf 
                    <div class="form-group">
                        <select name="point" class="form-control" id="">
                            <option value="">--Pilih Point yang diberikan--</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    @error('point')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <input type="hidden" value="{{$film->id}}" name="film_id">
                        <textarea name="content" class="form-control" cols="20" rows="10" placeholder="Isi Kritik">{{old('content')}}</textarea>
                    </div>
                    @error('content')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <input type="submit" class="btn btn-primary btn-sm" value="Post Kritik">

                </form>
      
            </div>

    </div>
    @endsection
    