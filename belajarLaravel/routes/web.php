<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@utama');
Route::get('/register', 'AuthController@bio');
Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', 'IndexController@tabel');


//CRUD CAST

//Create Data
//menuju ke form tambah cast
Route::get('/cast/create', 'CastController@create');
//untuk mengirim inputan ke database
Route::post('/cast', 'CastController@store');

//Read Data
//untuk menampilkan semua data di database di web browser
Route::get('/cast', 'CastController@index');
//deatil data berdasarkan id
Route::get('/cast/{cast_id}', 'CastController@show');

//Update Data
//menuju ke form edit cast berdasarkan id
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//untuk update berdasarkan id
Route::put('/cast/{cast_id}', 'CastController@update');

//Delete Data
Route::delete('/cast/{cast_id}', 'CastController@destroy');



//CRUD Genre
Route::resource('genre', 'GenreController');

//CRUD Film
Route::resource('film', 'FilmController');

//Tambah Kritik
Route::post('/kritik', 'KritikController@store');

Auth::routes();